function AppController ($scope) {
  //TODO: dynamically fetch data from server
    $scope.data = {
      "home": {
        "name": "Portugal",
        "score": "2"
      },
      "away": {
        "name": "Iran",
        "score": "0"
      }
    }
  };

  AppController.$inject = ['$rootScope'];

export default AppController;
