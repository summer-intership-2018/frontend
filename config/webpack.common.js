const path = require('path');

var webpack = require('webpack');
var helpers = require('./config-helpers');

module.exports = function(dev) {
  return {
    entry: {
      vendor: './app/vendor.js',
      app: './app/main.js'
    },
    resolve: {
      extensions: ['.js']
    },
    output: {
      path: helpers.root('dist'),
      publicPath: '/dist/',
      filename: '[name].js'
    },
    devServer: {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
          "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    module: {
      rules: [{
        test: /\.scss$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }, {
          loader: 'sass-loader'
        }]
      }]
    }
  }
}
